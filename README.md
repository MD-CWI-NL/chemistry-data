## Introduction

This repository contains cross-sections, and reaction rates for different species. Additionally scripts can be found that are made to read these files. Premade LXCat formatted files for each species are found in the `lxcat_cs_files` folder. Bolsig- has been run for different gas compositions and the input and output files can be found in the `bolsig` folder. These Bolsig- output files have been converted to afivo readable files and extended with reaction rates for different reactions. These afivo formatted input files can be found in `afivo_chemistry_input`.

## Cross-Sections

This repo contains species specific folders e.g. `O2`, `N2`, etc. In these folders you will find the source papers from which cross-sections were gathered and reaction specific cross-section files. 

### Format of Cross-Section files

The cross-section files have a filename which resembles the reaction that they represent e.g. `e_O2_->_e_e_O2+` being the ionization reaction of O2. This filename has a specific format (input and output species will be abreviated to is and os respectively): 

is1_is2_isN_->_os1_os2_osN(_elastic/momentum_transfer/effective)

with the part in brackets only applicable to those reactions.

When a cross-section file is opened 3 different line-starts are found:

- `#` are comment lines and are not used by any parser. These can contain the source papers with a doi link or other comments not necessary for using the data
- `%` are key-value lines and have the format `% key value`. Examples of used key-value pairs are: threshold (which is the energy threshold of the reaction), energy_unit (currently only 'eV' is expected), length_unit (can be 'cm' or 'm'), cs_factor (a factor to multiply the cross-sections with), m/M (the mass fraction of species used in elastic collisions). The implementation and use of these key-value pairs are parser specific. For an implementation of the parser see `scripts/read_data_utils.py`
- No line start. These are the data lines and have the format `energy cross-section`.

## Chemical Reactions

This repo contains folders called after the process they contain reactions of e.g. `attachment`, `negative_ion_conversion`, etc. In these folders you will find reaction specific files and you might find source papers although this might incur a lot of duplicates since multiple types of reactions are often contained in one paper.

### Format of the Reaction Files

The reaction files have the same filename structure as the cross-section files.

When a reaction file is opened 3 different line starts are found:

- `#` same as cross-section file
- `%` same as cross-section file
- No line start. **The first line** without a special character at the start contains the name of the analytical function e.g. `k2_func`. The following lines without a line starting character are data lines and have the format of `name value` which for the analytical functions are usually `c1 c1_val` where `c1` is referenced in the function description in the files contained in the `analytical_functions` folder.

## Analytical Functions

The `analytical_functions` folder contains files with the function name e.g. `k1_func`. These files contain the function form with input parameters `c1 c2 ... cN`. These functions are implemented in the simulation software. These implemented functions should be adressable by using the strings read from the reaction files (e.g. `k2_func`) like a dictionary/map with strings as keys and function pointers as values, but this is implementation specific.

## Scripts

Some scripts and utilities to work with the cross-section and reaction files are given in the `scripts` folder.

`read_data_utils.py` contains functions to read cross-section and reaction files which can be used to create the formatted files which your specific simulation software can read.

`convert_cs_folder_to_lxcat.py` is a script that can be run on a species specific folder e.g. O2 it will output all contained cross-section files in an LXCat style format. A call to this script can be as follows (from the main folder):

`python3 scripts/convert_cs_folder_to_lxcat.py O2 [-elastic elastic/momentum_transfer]`

The flag `-elastic` is there to let the user choose which cross section to add to the LXCat file for the "ELASTIC" collision type, either the cross-section file ending with "_elastic" or the one ending with "_momentum_transfer" (default).

`conversion_to_lxcat_utils.py` contains functions to transform the cross-section files to a format dictated by LXCat and understandable by programs expecting LXCat files (e.g. Bolsig+/-).

`conversion_to_afivo_utils.py` contains functions to transform chemical reaction files to a format that afivo derive simulation software understands.

`example_convert_all_to_afivo.py` is an example implementation which takes all the chemical reaction files found in the given folders and transforms them into a format that afivo derive simulation software can understand.