from collections import namedtuple
import numpy as np
from os import path

def merge_states_within_brackets(split_states):
    # This function will only work properly if there is 1 split state in the list.
    # For multiple you will have to adjust this function.

    idx_start_split = -1
    idx_end_split = -1
    for i, split_state in enumerate(split_states):

        if "(" in split_state and ")" not in split_state:
            idx_start_split = i
        elif ")" in split_state and "(" not in split_state:
            idx_end_split = i
    
    if idx_start_split >= 0 and idx_end_split >= 0:
        merged_state = split_states[idx_start_split]
        for i in range(idx_start_split + 1, idx_end_split + 1):
            merged_state += f',{split_states[i]}'
        
        corrected_states = [x for x in split_states[:idx_start_split]]
        corrected_states.append(merged_state)
        corrected_states += split_states[idx_end_split + 1:]

        return corrected_states
    else:
        return split_states


def read_reaction_filename(reaction_filename):

    reaction_filename_data = namedtuple("ReactionFilenameData", ["reaction", "input", "output", "type"])

    reaction = path.basename(reaction_filename)
    reaction_type = ""

    if "elastic" in reaction:
        reaction_type = "elastic"
        reaction = reaction.replace("_elastic", "")
    elif "momentum_transfer" in reaction:
        reaction_type = "elastic"
        reaction = reaction.replace("_momentum_transfer", "")
    elif "effective" in reaction:
        reaction_type = "effective"
        reaction = reaction.replace("_effective", "")

    input_species = []
    output_species = []

    reaction = reaction.replace("_->_", " -> ")

    reaction_species = reaction
    reaction_species = reaction_species.replace("_", " ")
    input_output_species = reaction_species.split("->")
    input_species = input_output_species[0].split()
    output_species = input_output_species[1].split()

    # Sometimes we will have a lot of stuff in between brackets like O2(state1, state2, etc.)
    # so we need to make sure it is counted in 1 species and not split
    output_species = merge_states_within_brackets(output_species)
    input_species = merge_states_within_brackets(input_species)

    if len(reaction_type) == 0:
        if input_species.count("e") > 0:
            if input_species.count("e") < output_species.count("e"):
                reaction_type = "ionization"
            elif input_species.count("e") > output_species.count("e"):
                reaction_type = "attachment"
            else:
                reaction_type = "excitation"
        else:
            reaction_type = "plasma-chemical"


    reaction = reaction.replace("_", " + ")


    reaction_filename_data.reaction = reaction
    reaction_filename_data.type = reaction_type
    reaction_filename_data.input = input_species
    reaction_filename_data.output = output_species

    return reaction_filename_data


def read_cross_section_file(cross_section_filename):

    cross_section_data = namedtuple("CrossSectionData", ["reaction", "cross_sections", "energy", "properties", "type", "input", "output"])

    cross_sections = []
    energy = []
    properties = {}

    reaction_filename_data = read_reaction_filename(cross_section_filename)

    with open(path.abspath(cross_section_filename)) as f:

        for line in f.readlines():
            
            if line[0] == "#":
                continue

            split_line = line.split()

            if split_line[0] == "%":
                cs_property = split_line[1]
                property_val = split_line[2]

                if cs_property in ["threshold", "m/M", "cs_factor"]:
                    property_val = float(property_val)
            
                properties[cs_property] = property_val
            else:
                energy.append(float(split_line[0]))
                cross_sections.append(float(split_line[1]))
        
    cross_section_data.cross_sections = np.array(cross_sections)
    cross_section_data.energy = np.array(energy)
    cross_section_data.properties = dict(properties)
    cross_section_data.reaction = reaction_filename_data.reaction
    cross_section_data.type = reaction_filename_data.type
    cross_section_data.input = reaction_filename_data.input
    cross_section_data.output = reaction_filename_data.output

    return cross_section_data


def read_plasma_chemical_file(plasma_chemical_filename):

    plasma_chemical_data = namedtuple("PlasmaChemicalData", ["reaction", "function", "function_data", "properties", "type", "input", "output"])

    reaction_filename_data = read_reaction_filename(plasma_chemical_filename)

    function = ""
    function_data = []
    properties = {}

    with open(path.abspath(plasma_chemical_filename)) as f:

        for line in f.readlines():
            
            if line[0] == "#":
                continue

            split_line = line.split()

            if split_line[0] == "%":
                cs_property = split_line[1]
                property_val = split_line[2]

                if "factor" in cs_property:
                    property_val = float(property_val)

                properties[cs_property] = property_val
            elif len(split_line) == 1:
                function = split_line[0]
            else:
                function_data.append(float(split_line[1]))

    plasma_chemical_data.reaction = reaction_filename_data.reaction
    plasma_chemical_data.type = reaction_filename_data.type
    plasma_chemical_data.input = reaction_filename_data.input
    plasma_chemical_data.output = reaction_filename_data.output
    plasma_chemical_data.function = function
    plasma_chemical_data.function_data = list(function_data)
    plasma_chemical_data.properties = dict(properties)

    return plasma_chemical_data