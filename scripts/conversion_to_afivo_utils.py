from os.path import basename
from read_data_utils import read_plasma_chemical_file

def chem_file_to_afivo(path_to_chem_file: str) -> str:

    plasma_chemical_file_data = read_plasma_chemical_file(path_to_chem_file)

    reaction = plasma_chemical_file_data.reaction
    function = plasma_chemical_file_data.function

    constants = list(plasma_chemical_file_data.function_data)
    constants = [str(x) for x in constants]
    constants = " ".join(constants)

    properties = dict(plasma_chemical_file_data.properties)

    length_unit = properties.get("length_unit", "m")

    return f"{reaction},{function},{constants},{length_unit}"