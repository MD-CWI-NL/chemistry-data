from os import listdir
from os.path import isdir, join
import argparse
from conversion_to_lxcat_utils import cross_section_file_to_lxcat_format

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="This script will traverse the passed folder name for cross section files and combine them all in an LXCat formatted file")
    parser.add_argument("CSFolder", type=str, help="Folder which contains the cross section files to be written in an LXCat format")
    parser.add_argument("-elastic", type=str, help="Type of elastic collision cross sections to use as THE elastic collision type. \"elastic\" or \"momentum_transfer\"", 
                        default="momentum_transfer")

    args = parser.parse_args()
    folder = args.CSFolder

    # Determines if we should include "_elastic" or "_momentum_transfer"
    # cross sections as our elastic collision type
    elastic_scattering_type = args.elastic

    files = sorted([x for x in listdir(folder) if not isdir(x) and not x.endswith(".pdf")])

    to_delete_elastic_type = "elastic" if elastic_scattering_type == "momentum_transfer" else "momentum_transfer"
    delete_non_preferred_elastic_type = False
    # We need to check if the preferred elastic scattering type is also in the set of cs files.
    # If it is not then we shouldnt delete the one that IS included.
    for filename in files:
        if elastic_scattering_type in filename:
            delete_non_preferred_elastic_type = True
            break
    
    if delete_non_preferred_elastic_type:
        for filename in files:
            if to_delete_elastic_type in filename:
                files.remove(filename)

    lxcat_format_folder = ""

    header = folder
    header = header.replace("_", " ")
    header = header.title() + "\n"

    header_stars = 10
    header = "*" * header_stars + " " + folder +  " " + "*" * header_stars + "\n"
    header += "This LXCat file was generated from the cross sections in the chemistry data repo found at:\n"
    header += "https://gitlab.com/MD-CWI-NL/chemistry-data \n"
    header += "*" * 2 * header_stars + "\n\n\n"

    lxcat_format_folder += header
    for i_file, cs_file in enumerate(files):
        path_to_cs_file = join(folder, cs_file)

        cs_lxcat_format = cross_section_file_to_lxcat_format(path_to_cs_file)
        lxcat_format_folder += cs_lxcat_format
    
    print(lxcat_format_folder)
