from os import listdir
from os.path import isdir, join
from conversion_to_afivo_utils import chem_file_to_afivo


if __name__ == "__main__":
    folders_to_include = ["attachment", "detachment", "electron_ion_recombination", "negative_ion_conversion",
                         "positive_ion_conversion", "positive_negative_ion_recombination", "chemical_transformation_neutrals"]

    for folder in folders_to_include:
        files = sorted([x for x in listdir(folder) if not isdir(x)])

        header = folder
        header = header.replace("_", " ")
        header = header.title()

        print(f"#### {header} ####")
        for i_file, chem_file in enumerate(files):
            path_to_chem_file = join(folder, chem_file)

            print(chem_file_to_afivo(path_to_chem_file))
