from read_data_utils import read_cross_section_file
import numpy as np
from datetime import datetime
from os import path


""" The LXCat Format:
CROSS SECTION DATA FORMAT
In downloaded files, each collision process is defined by a block consisting of
1st line
Keyword in capitals indicating the type of the collision. Possible collision types are elastic, effective, excitation,
ionization, or attachment (capital letters required, key words are case sensitive), where "elastic" is used to denote
the elastic momentum transfer cross section and where "effective" denotes the total momentum transfer cross section (sum
of elastic momentum transfer and total inelastic cross sections).  The latter is useful for solving the Boltzmann
equation in the 2-term approximation.

2nd line
Name of the target particle species. This name is a character string, freely chosen by the user, e.g. "Ar". Optionally
for excitation processes, the name of the corresponding excited state can be specified on the same line, separated from
the first name either by arrow "->" (dash + greater than) or by double-head arrow "<->" (less than + dash +
greater than), e.g. "Ar -> Ar*" and "Ar <-> Ar*", respectively. In the later case BOLSIG+ will automatically
define the inverse superelastic process, constructing the superelastic cross-section by detailed balancing, and
considering the indicated excited state as the target. In this case, the ratio of statistical weights must be input in
the 3rd line (see below).

3rd line
For elastic and effective collisions, the ratio of the electron mass to the target particle mass. For excitation or
ionization collisions, the electron energy loss (nominally the threshold energy) in eV. For attachment, the 3rd line is
missing. In case of an excitation process where an excited state has been indicated on the 2nd line using double-head
arrow "<->", the 3rd line must specify also ratio of the statistical weights of the final state to the initial state
as the second parameter in 3rd line this is needed by BOLSIG+ to calculate the de-excitation cross-section. The
statistical weight ratio, if given, will also be used by the automatic superelastics option in BOLSIG+. If this ratio is
not provided then BOLSIG+ will assume it unity.

from 4th line (optionally)
User comments and reference information, maximum 100 lines. The only constraint on format is that these comment lines
must not start with a number.

Finally
Table of the cross section as a function of energy. The table starts and ends by a line of dashes "------" (at least 5),
and has otherwise two numbers per line: the energy in eV and the cross section in m2.
"""



def cross_section_file_to_lxcat_format(cross_section_filename):

    cross_section_data = read_cross_section_file(cross_section_filename)

    properties = dict(cross_section_data.properties)
    energy = np.array(cross_section_data.energy)
    cross_sections = np.array(cross_section_data.cross_sections)
    reaction_type = str(cross_section_data.type)
    input_species = list(cross_section_data.input)
    output_species = list(cross_section_data.output)
    reaction = str(cross_section_data.reaction)

    # LXCat cross section unit is m2
    length_unit = properties.get("length_unit", "m")
    cross_sections *= 1e-4 if length_unit == "cm" else 1
    # Check if there was an additional factor in the cs file 
    # (for instance cross-sections written in units of 1e-16 cm2, where 1e-16 would be the cs_factor)
    cs_factor = properties.get("cs_factor", 1)
    cross_sections *= cs_factor

    # LXCat energy unit is eV
    energy_unit = properties.get("energy_unit", "eV")
    # Adjust this line if we ever have files with an energy unit different from eV
    energy *= 1 if energy_unit == "eV" else 1
    
    if reaction_type not in ["elastic", "effective", "excitation", "ionization", "attachment"]:
        print(f"LXCat does not recognize type: {reaction_type}")
        exit(-1)

    first_line = reaction_type.upper() + "\n"

    second_line = ""
    # We take the first non electron species as our target species
    for species in input_species:
        if species != "e":
            second_line = species
            break
    if reaction_type == "excitation":
        # We take the first non electron species from the output species as our excited species
        excited_species = ""
        for species in output_species:
            if species != "e":
                excited_species = species
                break
        second_line += " -> " + excited_species
    second_line += "\n"

    third_line = ""
    if reaction_type in ["elastic", "effective"]:
        mass_fraction = properties.get("m/M", 1)
        third_line += str(mass_fraction) + "\n"
    
    if reaction_type in ["ionization", "excitation"]:
        threshold = properties.get("threshold", 0)
        third_line += str(threshold) + "\n"
    
    fourth_line = ""
    fourth_line += "REACTION: " + reaction + "\n"
    fourth_line += "INPUT_SPECIES: " + " ".join(input_species) + "\n"
    fourth_line += "OUTPUT_SPECIES: " + " ".join(output_species) + "\n"
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    fourth_line += "CREATED ON: " + dt_string + "\n"
    fourth_line += "CS_FILENAME: " + path.basename(cross_section_filename) + "\n"
    fourth_line += "COLUMNS: Energy (eV) | Cross section (m2) \n"
    
    n_data_dashes = 30
    fifth_line = "-" * n_data_dashes + "\n"

    # Add extra 0 line at the beginning of the dataset. (THIS IS NOT IN THE SPEC)
    # Some programs use the last known value from a dataset when the requested point falls out of the range of the dataset.
    # This can have very big effects in simulations. This is why we add a 0 point outside of our range so that this does not happen.
    if "threshold" in properties:
        threshold = properties.get("threshold", 0)
        if energy[0] > threshold:
            fifth_line += " " + str(threshold) + "\t" + str(0.0) + "\n"
        else:
            if cross_sections[0] > 0.0:
                fifth_line += " " + str(0.95 * energy[0]) + "\t" + str(0.0) + "\n"



    for energy, cs in zip(energy, cross_sections):
        fifth_line += " " + str(energy) + "\t" + str(cs) + "\n"
    
    # Add extra 0 line (THIS IS NOT IN THE SPEC)
    if cross_sections[-1] != 0:
        fifth_line += " " + str(1.05 * np.max(energy)) + "\t" + str(0.0) + "\n"

    fifth_line += "-" * n_data_dashes + "\n\n"

    lxcat_format = first_line + second_line
    if len(third_line) > 0:
        lxcat_format += third_line
    lxcat_format += fourth_line
    lxcat_format += fifth_line

    return lxcat_format